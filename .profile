# $OpenBSD: dot.profile,v 1.7 2020/01/24 02:09:51 okan Exp $
#
# sh/ksh initialization

PATH=$HOME/bin:/bin:/sbin:/usr/bin:/usr/sbin:/usr/X11R6/bin:/usr/local/bin:/usr/local/sbin:/usr/games:$HOME/.cargo/bin
#export $PS1 = '\u \w '
HISTSIZE=10000
HISTFILE="$HOME"/.ksh_hist
HISTCONTROL=ignoredups 

#export TERM=xterm
export ENV="$HOME"/.kshrc
export LANG=en_US.UTF-8
export LC_CTYPE=en_US.UTF-8
export LC_ALL=en_US.UTF-8
export EDITOR=VIM
export PAGER=less
export BROWSER=w3m
#export PS1='  \u \w: '
export PS1=" \[\e[32m\]\u \w: \[\e[m\]"

export PATH HOME TERM ENV
