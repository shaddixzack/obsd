"reload, :source ~/.vimrc
colorscheme elflord
set nu
set cursorline
set ignorecase
syntax on

" Configure backspace so it acts as it should act
set backspace=eol,start,indent
set whichwrap+=<,>,h,l

