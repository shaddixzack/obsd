#!/bin/ksh
#export $PS1 = '\u \w '
#export TERM=xterm
#export TZ=Singapore
HISTSIZE=10000
HISTFILE="$HOME/.ksh_hist"
HISTCONTROL=ignoredups:ignorespace

us() { lynx --dump --width 200 https://cdn.openbsd.org/pub/OpenBSD/"$(uname -r)"/packages/"$(uname -p)"/index.txt | awk '{print $10}' | sed 's/-[^-]*$//g;/^[[:blank:]]*$/d' > "$HOME"/.pkgs && echo done ;}
as() { p="$(fzf -m --cycle --height=50% -q "${*}" -1 -0 < "$HOME"/.pkgs)" && pkg_info "${p}";}
s() { p="$(fzf --algo=v1 --cycle --height=50% -q "${*}" -1 -0 --preview="pkg_info {} | grep Description: -A 5" --preview-window=up < "$HOME"/.pkgs)" && doas pkg_add "${p}";}
alias a="doas pkg_add"
alias u="doas pkg_add -uV"
alias uu="doas pkg_add -UuV; off"
alias d="doas pkg_delete"
alias krc='doas vim "$HOME"/.kshrc; . "$HOME"/.kshrc'
alias xrc='doas vim "$HOME"/.xsession'
alias prc='doas vim "$HOME"/.profile'

f() { find "${2:-.}" -iname "*${1}*" ;}
l() { case "${*}" in "") ls -FAh ;; *) ls -FAh "${*}" 2> /dev/null || ( g="$(find "$PWD" | fzf --cycle --height=50% -q "${*}" -1 -0)" && ls -FAh "$g" ) ;; esac ;}
ll() { case "${*}" in "") ls -lFAh ;; *) ls -lFAh "${*}" 2> /dev/null || ( g="$(find "$PWD" | fzf --cycle --height=50% -q "${*}" -1 -0)" && ls -lFAh "$g" ) ;; esac ;}
lss() { du -had 1 "$PWD" | sort -rh | less -Mi ;}
alias lt='ls -lAFhtr | less -Mi' # recent last
alias lsn='ls -FAh | cat -n'
alias less="less -Mi"
alias dmci='doas make clean install'
alias mv='mv -v'
alias lll="ls -FAh | less -Mi"
alias cp='rsync -aP --info=progress2' #'cp -v'
alias cs='printf "\033c"'
#alias rm='rm -iv'
cpb() { fzf -m --cycle --height=50% < "${HOME}"/.kshrc | xsel -ipsb ;}
cph() { history | fzf -m --cycle --height=50% | sed 's/ *[0-9]* *//' | tr -d '\n' | xsel -ipsb ;} #bind '"\C-F":"fh\n"' # fzf history
cpl() { sel="$(find "$PWD" -type f | fzf --cycle --height=50% -q "${*}" -1 -0)" && fzf -m --cycle --height=50% < "$sel" | xsel -ipsb && unset sel ;}
cpy() { fzf -m --cycle --height=50% | tr -d '\n' | xsel -ipsb ;} ##xclip -sel c
pst() { xsel -opsb ;} ##xclip -o
alias df='df -h'
alias q='exit'
alias ..='cd .. ; ls -FAh'
alias ...='cd ../.. ; ls -FAh'
alias ....='cd ../../.. ; ls -FAh'
drive() { printf "%s\n\n" "$(sysctl hw.disknames)" "$(dmesg | grep '^[wsch]d.*[0-9]')" "$(usbdevs)" "##fat:mount_msdos /dev/sdXi /mnt/USB, format: newfs_msdos -F FAT32 /dev/rsdXi" ;}
#vnconfig vnd0 /downloads/bar.iso        mount -t cd9660 /dev/vnd0c /mnt
#umount /mnt   vnconfig -u vnd0
drv() { printf "%s\n" "$(doas disklabel -h "${*}")" ;} 
pk() { p="$(ps ax | fzf --algo=v1 --cycle --height=50% -q "${*}" | awk '{print $1}')" && kill -9 "${p}";}
alias mpv='mpv --audio-channels=2 --audio-display=no'
alias cmus='cmus || tmux attach -t 800 -d'
alias cmn='cmus-remote -n'
alias cmp='cmus-remote -r'
alias cmr='cmus-remote -c -l && cmus-remote -l "${HOME}"/mp3'
alias cm+='cmus-remote -C "vol +2%"'
alias cm-='cmus-remote -C "vol -3%"'
alias dlna='doas minidlnad -R -f /etc/minidlna.conf -P /var/run/minidlna/minidlna.pid -d'
alias pi='ping -c 3 192.168.1.1 && echo && ping -c 3 9.9.9.9 && echo && ping -c 3 quad9.net'
alias myip='curl icanhazip.com'
alias 800='ssh -vv -X pa@192.168.1.100 -t "tmux attach -t 800 -d || tmux new -s 800"'
alias g1='ssh -vv -X pa@192.168.1.101 -t "tmux attach -t g1 -d || tmux new -s g1"'
alias svd='ssh -vv -X pa@192.168.1.102 -t "tmux attach -t svd -d || tmux new -s svd"'
alias 390='ssh -vv -X pa@192.168.1.103 -t "tmux attach -t 390 -d || tmux new -s 390"'
alias v14='ssh -vv -X pa@192.168.1.104 -t "tmux attach -t v14 -d || tmux new -s v14"'
alias hpdf='ssh -vv -X pa@192.168.1.105 -t "tmux attach -t hpdf -d || tmux new -s hpdf"'
ssht() { ssh -vv -X "${@}" -t "tmux attach -t $(uname -n) -d || tmux new -s ssh" ;}
sshk() { ssh-keygen -N "${*}" -t ed25519 -f "$HOME/.ssh/id_ed25519" ;}
sshc() { cat "$HOME/.ssh/id_ed25519.pub" | ssh "${*}" 'mkdir ~/.ssh; cat >> "$HOME/.ssh/authorized_keys"' ;}
ssha() { eval "$(ssh-agent)"; ssh-add "$HOME/.ssh/id_ed25519" ;}

wpp() { sel="$(find "${HOME}"/pix/* -type f | sort -R | head -n 1)" && xwallpaper --zoom "$sel" ;}
xwp() { psel="$(sxiv -tfpo "${HOME}"/pix/*)" && xwallpaper --zoom "$psel" ;}

vu() { ( [ -n "${*}" ] && sndioctl output.level=0."${*}" output.mute=0 ) || sndioctl output.level=+0.02 output.mute=0 ;}
vd() { ( [ -n "${*}" ] && sndioctl output.level=0."${*}" ) || sndioctl output.level=-0.03 ;}
vm() { sndioctl output.mute=! ;}
#bu() { ( [ -n "${*}" ] && xbacklight ="${*}"% ) || xbacklight +2% ;}
#bd() { ( [ -n "${*}" ] && xbacklight ="${*}"% ) || xbacklight -3% ;}
alias off='shutdown -ph now || reboot -p'
alias on='doas reboot || shutdown -r now'

ym() { yt-dlp -f "(mp4)[height<=480]" -o - "$(xclip -o)" | mpv - --fs --force-seekable=yes --no-cache --speed=1.35 ;}
yd() { yt-dlp -f "(mp4)[height<480]" -o "%(title)s.%(ext)s" --restrict-filenames --write-sub --write-auto-sub --sub-lang "en.*" --downloader=aria2c --downloader "dash,m3u8:native" -- "${*}" ;}
ya() { yt-dlp -x -f bestaudio -o "%(title)s.%(ext)s" --restrict-filenames --downloader=aria2c -- "${*}" ;}
yp() { yt-dlp -f best -o "%(playlist_index)s.%(title)s.%(ext)s" --downloader=aria2c -- "${*}" ;}
yt() { yt-dlp -f best -o "%(title)s.%(ext)s" --restrict-filenames --downloader=aria2c -- "${*}" ;}
#mywan() { wget -cqO- https://ipleak.net/json/ || wget -cqO- icanhazip.com || wget -cqO- ifconfig.me || wget -cqO- l2.io/ip || wget -cqO- ip.tyk.nu || wget -cqO- eth0.me ;}
#mylan() { ip r | grep -oE '([0-9]{1,3}\.){3}[0-9]{1,3}' | tail -n1 || ifconfig | awk '/cast/ {printf $2}' ;}
#mytime() { wget -cqO- "http://worldtimeapi.org/api/timezone/Singapore.txt" ;}
wsp() { k="$(ifconfig | awk '/cast/ {printf $2}')" && qrencode -t utf8  http://"$k":50080 && python3 -m http.server 50080 ;} 
#wsh() { qrencode -t ANSI http://192.168.1.100:50080 && busybox httpd -p 50080 -f -v ;} ##must have index.html
alias nb='newsboat'
#alias nbt='newsboat -u "$HOME"/.newsboat/urls-torrents -C "$HOME"/.newsboat/config-torrents'
alias mb='mbsync -a'
alias mutt='neomutt'
alias ht='htop'
alias aria2c='aria2c -x7 -c'
alias urls='rm urls; wget -qO- https://gitlab.com/shaddixzack/dot/-/raw/main/urls > urls'
fl() { grep -e "\(\)\ {\ " -e alias "$HOME"/.kshrc | grep -i "${*}" | sort ;}
#alias urls='rm urls; curl -sL https://gitlab.com/shaddixzack/dot/-/raw/main/urls -o urls'
#alias urls='rm urls; wget -qO- https://github.com/shaddixzack/dot/raw/main/urls > urls'

alias scoff='sleep 0.1 && xset -display "$DISPLAY" dpms force off'
alias stcli='curl -s https://raw.githubusercontent.com/sivel/speedtest-cli/master/speedtest.py | python3.10 -'
alias lynx='lynx --useragent="Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_0) AppleWebKit/537.1 (KHTML, like Gecko) Chrome/21.0.1180.79 Safari/537.1 Lynx"'
alias weather='curl wttr.in/beranang'
#tide_pk() { lynx --width 150 --dump https://wisuki.com/tide/459/port-klang | grep -A3 -i $(date +"%d/%m/%+4Y") ;}
tide_tp() { lynx --width 150 --dump https://www.tide-forecast.com/locations/Tumpat/tides/latest | grep -A6 -i "Tumpat: $(date +"%A %d %B %+4Y")" ;}
tide_pd() { lynx --width 150 --dump https://www.tide-forecast.com/locations/Port-Dickson-Malaysia/tides/latest | grep -A6 -i "Port Dickson: $(date +"%A %d %B %+4Y")" ;}
tide_pk() { lynx --width 150 --dump https://www.tide-forecast.com/locations/Pelabuhan-Kelang-Malaysia/tides/latest | grep -A6 -i "Klang: $(date +"%A %d %B %+4Y")" ;}
m2u() { librewolf --kiosk --private-window https://www.maybank2u.com.my ;}
hbmy() { librewolf --kiosk --private-window https://www.hsbc.com.my ;}

#alias scrh='scrcpy --lock-video-orientation=3'
#alias scrhi='scrcpy --lock-video-orientation=1'
#alias scrv='scrcpy --lock-video-orientation=0'
#alias scrvi='scrcpy --lock-video-orientation=2'

#lsext() { find . -type f -iname '*.'${1}'' -exec ls -l {} \; ;}
#batchexec() { find . -type f -iname '*.'${1}'' -exec ${@:2}  {} \; ;}
#rpass() { cat /dev/urandom | tr -cd '[:graph:]' | head -c ${1:-12} ;}
#log() { echo [$(date +%Y-%m-%d\ %H:%M:%S)] $* ;}
#htmldecode() { : "${*//+/ }"; echo -e "${_//&#x/\x}" | tr -d\; ; }
#urldecode() { : "${*//+/ }"; echo -e "${_//%/\\x}"; }
#lower() { echo "${@}" | awk '{print tolower"($0)"}' ;}
#upper() { echo "${@}" | awk '{print toupper"($0)"}' ;}
#expandurl() { curl -sIL $1 | awk '/^Location/ || /^Localização/ {print $2}' ; }
calc(){ printf '%s\n' "scale=${2:-3};${1}" | bc -l ;}
#wi() { [ $# -ne 0 ] && printf '%s\n\n' "$(file "${@}")" "$(stat "${@}")" "$(hexdump -C "${@}" | head)" ;} #type strings ldd ltrace strace readelf objdump nm gdb radare2 ghidra
#csv2json() { python3 -c "import csv,json,sys;print(json.dumps(list(csv.reader(open(sys.argv[1])))))" "${*}" ;}
#decToBin() { echo "ibase=10; obase=2; $1" | bc ;}
#decTohex() { bc <<< "obase=16; $1" ;}
#biggest(){ du -k * | sort -nr | cut -f2 | head -20 | xargs -d "\n" du -sh; }
#top10() { history | awk '{print $2}' | sort | uniq -c | sort -rn | head ; }
#beep() { echo -e -n \\a ; }
#get_pid() { ps -ef | grep "${1}" | grep -v grep | awk '{ print $2; }' ;}
#to_lower() { printf "${@}\n" | tr '[A-Z]' '[a-z]' ;}
#to_upper() { printf "${@}\n" | tr '[a-z]' '[A-Z]' ;}
#rms() { for f in *; do mv "$f" "${f// /_}"; done; }
#to360p() { for f in *; do ffmpeg -i '$f' -vf scale=-1:360 -c:v libx264 -crf 25 -preset slower -c:a copy -scodec copy '$f'.mkv ; done ;}
#m4a2mp3() { for f in *.m4a; do ffmpeg -i "$f" -acodec libmp3lame -ab 320k "${f%.m4a}.mp3"; done ;}
#vid2mp3() { for f in *; do ffmpeg -i source_video.avi -vn -ar 44100 -ac 2 -ab 192 -f mp3 sound.mp3 ; done ;}
#wma2ogg() { find -name '*wma' -exec ffmpeg -i {} -acodec vorbis -ab 128k {}.ogg \; ;}
#webm2ogg() { for f in "$*"; do ffmpeg -v warning -i "$f" -vn -acodec copy "${f%.webm}.ogg" && rm "$f"; done ;}
#2worstmp3() { for f in "$*"; do ffmpeg -v warning -n -i "$f" -codec:a libmp3lame -qscale:a 9 output.mp3 && mv output.mp3 "$f-wq.mp3"; done;}
#2lqmp3() { for f in "$*"; do ffmpeg -v warning -n -i "$f" -codec:a libmp3lame -qscale:a 7 output.mp3 && mv output.mp3 "$f-lq.mp3"; done;}
#flv2mp4() { find . -name "*.flv" -type f -exec ffmpeg -n -i '{}' -acodec copy -vcodec copy -copyts '{}.mp4' ; ;}
#flac2mp3() { for F in *.flac; do ffmpeg -i "$F" -ab 320k "${F%.*}.mp3"; done ;}
#shot() { import -frame -strip -quality 75 "$HOME/$(date +%s).png" ;}
#clock() { while true; do printf "\033c"; echo "==========="; date +"%r"; echo "==========="; sleep 1; done ;}
#man() { env man "$*" | sed s/-/-/g | ${MANPAGER:-${PAGER:-less}}; }
#rms() { for f in *" "*; do g=$(echo -n "$f" | tr -s " []{}()" "_"); mv -v "$f" "$g"; done ;}
#rms() { for f in *"-"*; do g=$(printf '%s' "$f" | tr -cs "[:alnum:]" "_"); mv -v "$f" "$g"; done ;}
rms() { for f in *" "*; do g=$(printf '%s' "$f" | tr -cs "[:alnum:]" "_"); mv -v "$f" "$g"; done ;}
rmss() { f="$(find ./*" "* | fzf --cycle --height=60% -q "${*}")" && g=$(printf '%s' "${f##*/}" | tr -cs "[:alnum:]" "_") && mv -v "$f" "$g" && unset f g ;}
ffn() { f="$(find ./*" "* -type f | fzf --cycle --height=60% -q "${*}")" && g=$(printf '%s' "${f##*/}" | tr -cs "[:alnum:]" "_" | sed 's/\(.*\)_/\1\./') && mv -v "$f" "$g" && unset f g ;}

#h() { info "$@" || man "$@" || help "$@" || "$@" --help || /usr/bin/"$@" --help ;}
dict() { curl "dict://dict.org/d:${1%%/}";}
bk() { cp "$1" "${1}"-"$(date +%Y%m%d%H%M)".bk ; }
txt2qr() { xsel -opsb | qrencode -t ASCIIi ; xsel -opsb | qrencode -t UTF8i ;}
cpr () { rsync -WavPh "$1" "$2" ;}
mcd() { mkdir -p "$*" && cd "$*" || return; ls -FAh; }
p() { msel="$(find "$PWD" | fzf --algo=v1 --cycle --height=50% -q "${*}" -1 -0)" && mpv --audio-channels=stereo --audio-display=no --slang=eng,en "$msel" ;}
pl() { msel="$(ls -tr "$PWD" | awk 'FNR==5{print}')" && mpv --audio-channels=stereo --audio-display=no --fs=yes --speed=1.1 --slang=eng,en "$msel" ;}
le() { m="$(find "$PWD" -type f | fzf --cycle --height=50% -q "${*}" -1 -0)" && less -Mi "$m" ;}
v() { m="$(find "$PWD" -type f | fzf --cycle --height=50% -q "${*}" -1 -0)" && doas vim "$m" ;}
m() { f="$(find "$PWD" | fzf --algo=v1 --cycle --height=50% -q "${*}")" && mv -v "$f" "$HOME/sd1/";echo ;}
m1() { f="$(find . -maxdepth 1 | fzf --algo=v1 --cycle --height=50%)" && g="$(find .. -type d | fzf --algo=v1 --cycle --height=50%)" && mv -v "$f" "$g" && unset f g;echo ;}
t() { [ -d "${HOME}"/.trash ] || mkdir -p "${HOME}"/.trash ; f="$(du -ha "$PWD" | sort -hr | fzf --algo=v1 --cycle --height=50% | cut -f2)" && mv -v "$f" "${HOME}"/.trash/ ;}
srt1() { f="$(fzf --cycle --prompt="SRT>")" && g="$(fzf --cycle --prompt="MKV/MP4>")" && h="${g%.*}" && cp -v "$f" "$h".srt ;}
c() { case "${*}" in "") ls -FAh ;; *) cd "${*}" || g="$(find "$PWD" -type d | fzf --cycle --height=50% -q "${*}" -1 -0)" && cd "${g}" && unset g;ls -FAh ;; esac ;}
wb() { w3m -o auto_image=TRUE "https://wiby.me/?q='$*'" ;}
qw() { w3m -o auto_image=TRUE "https://www.qwant.com/?q='$*'" ;}
s1() { w3m -o auto_image=TRUE "https://searx.be/search?q='${*}'" ;}
s2() { w3m -o auto_image=TRUE "https://searx.prvcy.eu/searxng/search?q='${*}'" ;}
s3() { w3m -o auto_image=TRUE "https://search.ononoki.org/search?q='${*}'" ;}
s4() { w3m -o auto_image=TRUE "https://searx.kutay.dev/search?q='${*}'" ;}
s5() { w3m -o auto_image=TRUE "https://searx.headpat.exchange/search?q='${*}'" ;}
s6() { w3m -o auto_image=TRUE "https://paulgo.io/search?q='${*}'" ;}
s7() { w3m -o auto_image=TRUE "http://searxng.nicfab.eu/searxng/search?q='{*}'" ;}
s8() { w3m -o auto_image=TRUE "https://northboot.xyz/search?q='${*}'" ;}
s9() { w3m -o auto_image=TRUE "https://search.sapti.me/search?q='${*}'" ;}
s10() { w3m -o auto_image=TRUE "https://search.rhscz.eu/search?q='${*}'" ;}
s11() { w3m -o auto_image=TRUE "https://opnxng.com/search?q='${*}'" ;}
s12() { w3m -o auto_image=TRUE "https://search.mdosch.de/search?q='${*}'" ;}
ch() { wget -qO- cheat.sh/"${*}" ;}
ff() { w3m "http://www.frogfind.com/?q='${*}'" ;}
wk() { lynx -dump -width 120 "https://en.wikipedia.org/w/index.php?go=Go&search='${*}'" | grep -i 'search results' -A 50 ;}
ldn() { w3m -o auto_image=TRUE "https://www.die.net/search/?q=ksh'$*'" ;}
wka() { w3m -o auto_image=TRUE "https://wiki.archlinux.org/index.php?search='$*'" ;}
#yd() { w3m -o auto_image=TRUE "https://yandex.com/search/?text='$*'" ;}
#mg() { w3m -o auto_image=TRUE "https://metager.org/meta/meta.ger3?eingabe='$*'" ;}
#ff() { lynx --dump "http://www.frogfind.com/?q='${*}'" | grep -vE '^_*$' | grep -i 'search results for' -A 80 | less -Mi ;}
#subdl() { w3m -o auto_image=TRUE https://www.opensubtitles.org/en/search2/sublanguageid-eng/moviename-"${*}" ;}
#es() { links "https://www.ecosia.org/search?method=index&q='$*'" ;}
#gw() { links "https://search.givewater.com/serp?q='$*'" ;}
#wa() { links "https://www.wolframalpha.com/input?i='$*'" ;}
#br() { links "https://boardreader.com/s/'$*'.html;language=English" ;}
#gi() { links "https://gibiru.com/results.html?q='$*'" ;}
#lk() { links "https://www.lukol.com/s.php?q='$*'" ;}
#gb() { links "https://www.gigablast.com/search?c=main&qlangcountry=en-us&q='$*'" ;}
grd() { lynx "gopher://gopherddit.com/7/cgi-bin/reddit.cgi?$*" ;}
gwk() { lynx "gopher://gopherpedia.com/7/lookup?$*" ;}
gpb() { lynx "gopher://bay.parazy.de:666/7/q.dcgi?$*" ;}
gvs() { lynx "gopher://gopher.floodgap.com/7/v2/vs?$*" ;}
gfg() { lynx gopher://gopher.floodgap.com/ ;}
ghn() { lynx gopher://hngopher.com/ ;}
#gna() { lynx gopher://gopher.leveck.us/ ;}
#g4c() { lynx gopher://khzae.net:70/1/chan ;}
#gmu() { lynx gopher://mozz.us:70 ;}
#gcv() { lynx gopher://codevoid.de:70 ;}
#gpb() { lynx "gopher://bay.parazy.de:666/1/index.dcgi" ;}
#gopher://gopher.quux.org/
#gopher://home.jumpjet.info/
#gopher://gopher.floodgap.com/1/groundhog/
#gopher://dante.pglaf.org:70
#gopher://codevoid.de:70/1/cnn
#gopher://tilde.club/1/~mz721

ts() { transmission-daemon -b && sleep 0.3 && transmission-remote -l ;}
tk() { pkill transmission-daemon ;}
tl() { printf "\033c" ; transmission-remote -l ;}
ta() { transmission-remote -a "$@" ;}
td() { transmission-remote -t "$@" -r ; transmission-remote -l ;}
th() { transmission-remote -a magnet:?xt=urn:btih:"$*" --start; sleep 0.5; transmission-remote -l ;}
ti() { transmission-remote -t "$@" -if || transmission-remote -t "$@" -i ;}
tc() { find "${HOME}"/dl -type f \( -iname "*.jpg" -o -iname "*.jpeg" -o -iname "*.png" -o -iname "*.txt" -o -iname "*.nfo" -o -iname "*.m3u" \) -exec rm -fv {} \; ;}
#tg() { transmission-remote -t "${1}" -g "${2}" ;} #g=get
#tG() { transmission-remote -t "${1}" -G all ;}    #G=no-get

##extract archive fn with appropriate progs
extract() {
n="$(find "$PWD" | fzf -1 -0 --cycle --height=50% -q "${*}")" &&
case "$n" in
  *.tar.bz2|*.tar.gz|*.tar.xz|*.tbz2|*.tgz|*.txz|*.tar) 
           tar xvf "$n"     ;;
  *.lzma)  unlzma "$n"      ;;
  *.bz2)   bunzip2 "$n"     ;;
  *.rar)   unrar x -ad "$n" ;;
  *.gz)    gunzip "$n"      ;;
  *.zip)   unzip "$n"       ;;
  *.z)     uncompress "$n"  ;;
  *.7z|*.arj|*.cab|*.chm|*.deb|*.dmg|*.iso|*.lzh|*.msi|*.rpm|*.udf|*.wim|*.xar)
           7z x "$n"        ;;
  *.xz)    unxz "$n"        ;;
  *.exe)   cabextract "$n"  ;;
  *) echo " '$n' - unknown archive method"; return 1 ;;
esac ;}

#txtund=$(tput sgr 0 1)    # Underline
#txtbld=$(tput bold)       # Bold
#txtred=$(tput setaf 1)    # Red
#txtgrn=$(tput setaf 2)    # Green
#txtylw=$(tput setaf 3)    # Yellow
#txtblu=$(tput setaf 4)    # Blue
#txtpur=$(tput setaf 5)    # Purple
#txtcyn=$(tput setaf 6)    # Cyan
#txtwht=$(tput setaf 7)    # White
#txtrst=$(tput sgr0)       # Text reset

#bld='\[\033[01;1m\]'   # Bold
#itl='\[\033[01;3m\]'   # Italic
#udl='\[\033[01;4m\]'   # Underline
#sbl='\[\033[01;5m\]'   # SlowBlink
#blk='\[\033[01;30m\]'   # Black
#red='\[\033[01;31m\]'   # Red
#grn='\[\033[01;32m\]'   # Green
#ylw='\[\033[01;33m\]'   # Yellow
#blu='\[\033[01;34m\]'   # Blue
#pur='\[\033[01;35m\]'   # Purple
#cyn='\[\033[01;36m\]'   # Cyan
#wht='\[\033[01;37m\]'   # White
#clr='\[\033[00m\]'      # Reset
#osc='\033]52;c;'       #osc52

